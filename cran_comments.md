##Resubmission v. 0.1.2
This is a resubmission to the initial package version 0.1.1.

Introduced changes
* suppress installation of all packages in the vignette (I had overlooked one, sorry for that)
* checked all examples in the help files: they do not install any package

## Test environments for version 0.1.2
* local OS X install (R 4.0.2, R-devel 2020-07-01 r78761)
* win-builder (4.0.2, R-devel 2020-07-11 r788124)


## R CMD check results
There were no ERRORs or WARNINGs. 

* checking CRAN incoming feasibility ... NOTE
Maintainer: ‘Katharina Baum <katharina.baum@hpi.de>’

New submission
This is correct, it is (the resubmission of) a new submission.



## Downstream dependencies
There are currently no downstream dependencies for this package.